import Vue from 'vue'

export default {
	state: {
		movies: []
	},
	mutations: {
		SET_MOVIES(state, payload) {
			state.movies = payload
		}
	},
	actions: {
		LOAD_MOVIES({commit}) {
			Vue.$db.collection('favourite-movies')
			.get()
			.then(querySnapshot => {
				let movies = [];
				querySnapshot.forEach(s => {
					const data = s.data();
					let movie = {
						id: s.id,
						title: data.title,
						poster_path: data.poster_path
					};
					movies.push(movie)
				});
				commit('SET_MOVIES', movies)
			})
			.catch(error => console.log(error))
		}
	},
	getters: {
		getMovies: (state) => state.movies
	}
}