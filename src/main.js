import Vue from 'vue'
import App from './App.vue'
import VeeValidate from 'vee-validate';
import router from './router'
import store from './store'
import firebase from 'firebase'
import 'firebase/firestore'
import firebaseConfig from './config/firebase'

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();

db.settings({
	timestampsInSnapshots: true
});

Vue.$db = db;
Vue.use(VeeValidate);

new Vue({
	router,
	store,
	render: h => h(App),
	created() {
		firebase.auth().onAuthStateChanged(user => {
			this.$store.dispatch('STATE_CHANGED', user);
			this.$store.dispatch('LOAD_MOVIES');
		});
	}
}).$mount('#app');