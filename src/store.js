import Vue from 'vue'
import Vuex from 'vuex'
import userModule from './store/user'
import commonModule from './store/common'
import moviesFavouriteModule from './store/moviesFavourite'

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		userModule,
		commonModule,
		moviesFavouriteModule
	}
})
