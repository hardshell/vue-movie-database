import Vue from 'vue';
import axios from 'axios';

Vue.use(axios);

export let settings = {
    apiKey: 'a70dbfe19b800809dfdd3e89e8532c9e',
		//posterUrl: 'https://image.tmdb.org/t/p/w500'
};

export const appUrls = {
    getMoviesPopular: (current) => {
	    return axios.get('https://api.themoviedb.org/3/movie/popular?api_key='+settings.apiKey+'&page='+current)
    },
		getMoviesUpcoming: (current) => {
			return axios.get('https://api.themoviedb.org/3/movie/upcoming?api_key='+settings.apiKey+'&page='+current)
		},
    getMoviesItem: (id) => {
      return axios.get('https://api.themoviedb.org/3/movie/' + id + '?api_key='+settings.apiKey);
    },
		// getMoviesItemVideo: (id) => {
		// 	return axios.get('https://api.themoviedb.org/3/movie/' + id + '/videos' + '?api_key='+settings.apiKey);
		// },
		getMoviesSimilar: (id) => {
    	return axios.get('https://api.themoviedb.org/3/movie/' + id + '/similar' + '?api_key='+settings.apiKey);
		},
    searchMovies: (keyword) => {
      return axios.get('https://api.themoviedb.org/3/search/movie?query='+ keyword + '&api_key='+settings.apiKey);
    }
};

export const genresList = [ { id: 28, name: 'Action'},
	{ id: 12, name: 'Adventure'},
	{ id: 16, name: 'Animation'},
	{ id: 35, name: 'Comedy'},
	{ id: 80, name: 'Crime'},
	{ id: 99, name: 'Documentary'},
	{ id: 18, name: 'Drama'},
	{ id: 10751, name: 'Family'},
	{ id: 14, name: 'Fantasy'},
	{ id: 36, name: 'History'},
	{ id: 27, name: 'Horror'},
	{ id: 10402, name: 'Music'},
	{ id: 9648, name: 'Mystery'},
	{ id: 10749, name: 'Romance'},
	{ id: 878, name: 'Science Fiction'},
	{ id: 10770, name: 'TV Movie'},
	{ id: 53, name: 'Thriller'},
	{ id: 10752, name: 'War'},
	{ id: 37, name: 'Western'}
];