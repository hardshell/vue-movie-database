import Vue from 'vue'
import Router from 'vue-router'
import Store from './store'
import SignUp from './views/SignUp.vue'
import SignIn from './views/SignIn.vue'
import MoviesPopular from './views/MoviesPopular.vue'
import MoviesUpcoming from './views/MoviesUpcoming.vue'
import MoviesFavourite from './views/MoviesFavourite.vue'
import MoviesItem from './views/MoviesItem.vue'
import NotFound from './views/NotFound.vue'

Vue.use(Router);

export default new Router({
	mode: 'history',
	routes: [
		{ name: 'signup', path: '/signup', component: SignUp },
		{ name: 'signin', path: '/', component: SignIn },
		{ name: 'popular', path: '/popular', component: MoviesPopular, beforeEnter: AuthGuard},
		{ name: 'upcoming', path: '/upcoming', component: MoviesUpcoming, beforeEnter: AuthGuard },
		{ name: 'favourites', path: '/favourites', component: MoviesFavourite, beforeEnter: AuthGuard},
		{ name: 'movie', path: '/movie/:id', component: MoviesItem, beforeEnter: AuthGuard },
		{ name: '404', path: '*', component: NotFound },
	],
	scrollBehavior (to, from) {
		return { x: 0, y: 0 }
	}
});

function AuthGuard(from, to, next) {
	if(Store.getters.isUserAuthenticated)
		next()
	else
		next('/')
}